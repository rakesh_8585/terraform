locals {
    role = "arn:aws:iam::${var.account}:role/JenkinsRakesh"
}
variable "account" {}
variable "vpc_id" {}
variable "sg_name" {}
resource "aws_security_group" "new_sg" {
  vpc_id = var.vpc_id
  name        = var.sg_name
  description = "Security Group from Jenkins"
  ingress {
    description = "SSH from VPC"
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["10.0.0.0/8"]
  }
  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
  tags = {
    Name = "Rakesh SSH"
  }
}
variable "region" {}
variable "instance_type" {}
variable "key_name" {}
variable "vpc_security_group_ids" {}
variable "subnet_id" {}
variable "ami" {}
variable "tag_name" {}

data "aws_ami" "pulse" {
most_recent = true
owners = ["self"]
  filter {
      name   = "name"
      #values = ["Gold-AMI*"]
      values = [ var.ami ]
  }
}
provider "aws" {
  assume_role {
    role_arn = local.role
    session_name = "SESSION_NAME"
    external_id  = "EXTERNAL_ID"
  }
  region = var.region
}
#variable "new_sg" {
#  default = "${aws_security_group.new_sg.id}"
#}
resource "aws_instance" "example" {
  ami = "${data.aws_ami.pulse.id}"
  instance_type = var.instance_type
  key_name = var.key_name
  subnet_id = var.subnet_id
  #vpc_security_group_ids = [ var.vpc_security_group_ids ]
  #vpc_security_group_ids = [ "${aws_security_group.new_sg.id}" ]
  vpc_security_group_ids = [ coalesce(var.vpc_security_group_ids, "${aws_security_group.new_sg.id}") ]
  tags = {
  Name = var.tag_name
  }
}

output "instance_ip_addr" {
  value       = aws_instance.example.private_ip
  description = "The private IP address of the instance."
}

output "InstanceId" {
  value       = "${element(aws_instance.example.*.id, 0)}"
  description = "Instance ID"
}